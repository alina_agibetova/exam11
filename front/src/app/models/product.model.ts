export class Product {
  constructor(
    public _id: string,
    public title: string,
    public price: number,
    public description: string,
    public image: string,
    public category: {
      _id: string,
      title: string
    },
    public user:{
      _id:string,
      name:string,
      phone: string
    }
  ){}
}

export interface ProductData {
  [key: string]: any;
  _id: string,
  title: string;
  price: number;
  description: string;
  image: File | null;
  category: {
    _id: string,
    title: string
  },
  user:{
    _id:string,
    name:string,
    phone: string
  }
}

export interface ApiProductData {
  _id: string,
  title: string,
  price: number,
  description: string,
  image: string,
  category: {
    _id: string,
    title: string
  },
  user:{
    _id:string,
    name:string,
    phone: string
  }
}
