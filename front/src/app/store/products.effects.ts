import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createProductRequest,
  createProductsFailure,
  createProductSuccess,
  deleteProductFailure,
  deleteProductRequest,
  deleteProductSuccess,
  fetchProductFailure,
  fetchProductRequest,
  fetchProductsFailure,
  fetchProductsRequest,
  fetchProductsSuccess,
  fetchProductSuccess
} from './products.actions';
import { catchError, map, mergeMap, tap } from 'rxjs';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { ProductService } from '../services/product.service';


@Injectable()
export class ProductsEffects {
  fetchProducts = createEffect(() => this.actions.pipe(
    ofType(fetchProductsRequest),
    mergeMap(id => this.productService.getCategoriesById(id.id).pipe(
      map(products => fetchProductsSuccess({products})),
      catchError(() => of(fetchProductsFailure({error: 'Something went wrong'})))
    ))
  ));

  fetchProductById = createEffect(() => this.actions.pipe(
    ofType(fetchProductRequest),
    mergeMap(id => this.productService.getProductById(id.id).pipe(
      map(product => fetchProductSuccess({product})),
      catchError(() => of(fetchProductFailure({error: 'Something went wrong'})))
    ))
  ));

  createProduct = createEffect(() => this.actions.pipe(
    ofType(createProductRequest),
    mergeMap(({productData}) => this.productService.createProduct(productData).pipe(
      map(() => createProductSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createProductsFailure({error: 'Wrong data'})))
    ))
  ));

  deleteProductById = createEffect(() => this.actions.pipe(
    ofType(deleteProductRequest),
    mergeMap(id => this.productService.deleteProduct(id.id).pipe(
      map(product => deleteProductSuccess({product})),
      catchError(() => of(deleteProductFailure({error: 'Something went wrong'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private productService: ProductService,
    private router: Router
  ) {}
}
