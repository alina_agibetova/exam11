import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import {
  loginUserFailure,
  loginUserRequest, loginUserSuccess, logoutUser, logoutUserRequest,
  registerUserFailure,
  registerUserRequest,
  registerUserSuccess
} from './users.actions';
import { map, mergeMap, NEVER, tap, withLatestFrom } from 'rxjs';
import { HelpersService } from '../services/helpers.service';
import { Store } from '@ngrx/store';
import { AppState } from './types';


@Injectable()
export class UsersEffects {
  constructor(
    private actions: Actions,
    private userService: UserService,
    private router: Router,
    private helpers: HelpersService,
    private store: Store<AppState>
  ) {}

  registerUser = createEffect(() => this.actions.pipe(
    ofType(registerUserRequest),
    mergeMap(({userData}) => this.userService.registerUser(userData).pipe(
      map(user => registerUserSuccess({user})),
      tap(() => {
        this.helpers.openSnackbar('Register successful')
        void this.router.navigate(['/'])
      }),
      this.helpers.catchServerError(registerUserFailure)
    ))
  ));

  loginUser = createEffect(() => this.actions.pipe(
    ofType(loginUserRequest),
    mergeMap(({userData}) => this.userService.login(userData).pipe(
      map(user => loginUserSuccess({user})),
      tap(() => {
        this.helpers.openSnackbar('Login successful')
        void this.router.navigate(['/'])
      }),
      this.helpers.catchServerError(loginUserFailure)
    ))
  ));

  logoutUser = createEffect(() => this.actions.pipe(
    ofType(logoutUserRequest),
    withLatestFrom(this.store.select( state => state.users.user)),
    mergeMap(([_, user]) => {
      if (user){
        return this.userService.logout(user.token).pipe(
          map(() => logoutUser()),
          tap(() => this.helpers.openSnackbar('Logout successful'))
        );
      }
      return NEVER;
    })
  ))

}
