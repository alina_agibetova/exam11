import { createAction, props } from '@ngrx/store';
import { Product, ProductData } from '../models/product.model';

export const fetchProductsRequest = createAction(
  '[Products] Fetch Request',
  props<{id: string}>()
);
export const fetchProductsSuccess = createAction(
  '[Products] Fetch Success',
  props<{products: Product[]}>()
);
export const fetchProductsFailure = createAction(
  '[Products] Fetch Failure',
  props<{error: string}>()
);

export const createProductRequest = createAction(
  '[Products] Create Request',
  props<{productData: ProductData}>()
);
export const createProductSuccess = createAction(
  '[Products] Create Success'
);
export const createProductsFailure = createAction(
  '[Products] Create Failure',
  props<{error: string}>()
);


export const fetchProductRequest = createAction(
  '[Product] Fetch Request',
  props<{id: string}>()
);
export const fetchProductSuccess = createAction(
  '[Product] Fetch Success',
  props<{product: Product}>()
);
export const fetchProductFailure = createAction(
  '[Product] Fetch Failure',
  props<{error: string}>()
);


export const deleteProductRequest = createAction(
  '[Product] Delete Request',
  props<{id: string}>()
);
export const deleteProductSuccess = createAction(
  '[Product] Delete Success',
  props<{product: Product}>()
);
export const deleteProductFailure = createAction(
  '[Product] Delete Failure',
  props<{error: string}>()
);
