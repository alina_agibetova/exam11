import { LoginError, RegisterError, User } from '../models/user.model';
import { Product } from '../models/product.model';
import { Category } from '../models/category.model';

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError
}

export type ProductState = {
  products: Product[],
  product: Product | null,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  deleteLoading: boolean,
  deleteError: null | string
};

export type CategoryState = {
  categories: Category[],
  fetchLoading: boolean,
  fetchError: null | string,
};

export type AppState = {
  users: UsersState,
  products: ProductState,
  categories: CategoryState
}
