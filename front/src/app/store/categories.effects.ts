import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, of } from 'rxjs';
import { ProductService } from '../services/product.service';
import { fetchCategoriesFailure, fetchCategoriesRequest, fetchCategoriesSuccess } from './categories.actions';

@Injectable()
export class CategoriesEffects {
  fetchCategories = createEffect(() => this.actions.pipe(
    ofType(fetchCategoriesRequest),
    mergeMap(() => this.productService.getCategories().pipe(
      map(categories => fetchCategoriesSuccess({categories})),
      catchError(() => of(fetchCategoriesFailure({error: 'Something went wrong'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private productService: ProductService
  ) {}
}
