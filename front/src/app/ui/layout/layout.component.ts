import { Component, OnInit } from '@angular/core';
import { map, Observable, shareReplay } from 'rxjs';
import { User } from '../../models/user.model';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ActivatedRoute } from '@angular/router';
import { logoutUserRequest } from '../../store/users.actions';
import { Product } from '../../models/product.model';
import { fetchProductsRequest } from '../../store/products.actions';
import { Category } from '../../models/category.model';
import { fetchCategoriesRequest } from '../../store/categories.actions';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.sass']
})
export class LayoutComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  user: Observable<null | User>;
  products: Observable<Product[]>;
  categories: Observable<Category[]>;
  newUser!: User | null;

  constructor(private breakpointObserver: BreakpointObserver, private store: Store<AppState>, private route: ActivatedRoute) {
    this.categories = store.select(state => state.categories.categories);
    this.user = store.select(state => state.users.user);
    this.products = store.select(state => state.products.products);
  }

  logout() {
    this.store.dispatch(logoutUserRequest())
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch(fetchCategoriesRequest({id: params['id']}));
    })
    this.user.subscribe(user => {
      this.newUser = user;
    });

  }

}
