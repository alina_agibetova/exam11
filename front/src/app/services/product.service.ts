import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiProductData, Product, ProductData } from '../models/product.model';
import { map } from 'rxjs/operators';
import { CategoryData } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getProducts(){
    return this.http.get<ApiProductData[]>('http://localhost:8000/products').pipe(
      map(response => {
        return response.map(productData => {
          return new Product(
            productData._id,
            productData.title,
            productData.price,
            productData.description,
            productData.image,
            productData.category,
            productData.user
          );
        });
      })
    )
  }

  getProductById(id: string){
    return this.http.get<ApiProductData>(`http://localhost:8000/products/${id}`).pipe(
      map(result => {
        return result;
      })
    );
  }

  getCategoriesById(id: string){
    return this.http.get<ApiProductData[]>(`http://localhost:8000/products?category=${id}`).pipe(
      map(result => {
        return result.map(productData => {
          return new Product(
            productData._id,
            productData.title,
            productData.price,
            productData.description,
            productData.image,
            productData.category,
            productData.user
          );
        });
      })
    );
  }

  getCategories(){
    return this.http.get<CategoryData[]>(`http://localhost:8000/categories`).pipe(
      map(result => {
        return result
      })
    );
  }

  createProduct(productData: ProductData){
    const formData = new FormData();
    Object.keys(productData).forEach(key => {
      if (productData[key] !== null){
        formData.append(key, productData[key]);
      }
    });
    return this.http.post('http://localhost:8000/products', formData);
  }

  deleteProduct(id: string){
    return this.http.delete<ApiProductData>(`http://localhost:8000/products/${id}`);
  }
}
