import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Product, ProductData } from '../../models/product.model';
import { createProductRequest } from '../../store/products.actions';
import { Category } from '../../models/category.model';
import { fetchCategoriesRequest } from '../../store/categories.actions';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.sass']
})
export class NewProductComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<string | null>;
  products: Observable<Product[]>;
  categories: Observable<Category[]>

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.categories = store.select(state => state.categories.categories);
    this.products = store.select(state => state.products.products)
    this.loading = store.select(state => state.products.createLoading);
    this.error = store.select(state => state.products.createError);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch(fetchCategoriesRequest({id: params['id']}));
    })
  }

  onSubmit() {
    const productData: ProductData = this.form.value;
    this.store.dispatch(createProductRequest({productData}));
  }
}
