import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../../models/user.model';
import { ApiProductData, Product } from '../../../models/product.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/types';
import { ActivatedRoute } from '@angular/router';
import { deleteProductRequest, fetchProductRequest } from '../../../store/products.actions';
import { Category, CategoryData } from '../../../models/category.model';
import { fetchCategoriesRequest } from '../../../store/categories.actions';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.sass']
})
export class ProductComponent implements OnInit {
  user: Observable<User | null>;
  product: Observable<Product | null>;
  products: Observable<Product[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  thisProduct!: ApiProductData;
  deleteLoading: Observable<boolean>;
  deleteError: Observable<null | string>;
  newUser!: User | null;
  thisCategory!: Category | null;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.products = store.select(state => state.products.products);
    this.user = store.select(state => state.users.user);
    this.product = store.select(state => state.products.product);
    this.loading = store.select(state => state.products.fetchLoading);
    this.error = store.select(state => state.products.fetchError);
    this.deleteLoading = store.select(state => state.products.deleteLoading);
    this.deleteError = store.select(state => state.products.deleteError)

  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch(fetchProductRequest({id: params['id']}));
      this.product.subscribe(product => {
        this.thisProduct = <ApiProductData>product;
      });
    });
    this.user.subscribe(user => {
      this.newUser = user;
    });
    this.product.subscribe(category => {
      this.thisCategory = category;
    })
  }

  deleteProduct(id: string) {
    this.route.params.subscribe(params => {
      this.store.dispatch(deleteProductRequest({id: params['id']}));
    });
  }
}
