const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
  category: {
    type: Schema.Types.ObjectId,
    ref: 'Category'
  },
  title: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  description: String,
  image: String,
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
});

const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;