const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Product = require('../models/Product');
const Category = require("../models/Category");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },

  filename: (req, file, cb)=>{
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    if (req.query.category._id){
      const productByCategory = await Product.find({category: {_id:req.query.category}}).populate('user', 'name');
      return res.send(productByCategory);
    }
    const products = await  Product.find().populate('user', 'name');
    return res.send(products);
  } catch (e) {
    next(e);
  }
});


router.get('/:id', async (req, res, next) => {
  try {
    const product = await Product.findOne({_id: req.params.id}).populate('user', 'name');
    if (!product){
      return res.status(404).send({message: 'Not found'});
    }
    return res.send(product).populate('user', 'name');
  } catch (e){
    next(e);
  }
});


router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    const productData = {
      category: req.body.category,
      title: req.body.title,
      price: parseFloat(req.body.price),
      description: req.body.description,
      image: null,
      user: req.body.user,
    };

    if (req.file){
      productData.image = req.file.filename;
    }
    const product = new Product(productData);
    await product.save();
    return res.send(product);
  } catch (e){
    next(e);
  }
});

router.delete('/:id', async (req,res,next) => {
  try{
    const productById = await Product.deleteOne({_id: req.params.id});
    return res.send(productById);
  } catch (e){
    next(e);
  }
})

module.exports = router;