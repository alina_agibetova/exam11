const mongoose = require('mongoose');
const config = require('./config');
const Product = require('./models/Product');
const Category = require('./models/Category');
const User = require('./models/User')

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections){
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const user = await User.create({
    email: 'test@test.ru',
    password: '123',
    name: 'user',
    token: "yf1ffwWppK5cdJ3yUXvBQ"
  });

  const [jacket, dress] = await Category.create({
    title: 'Jacket',
  }, {
    title: 'Dress',
  });

  await Product.create({
    category: jacket,
    user: user,
    title: 'Jacket',
    price: 500,
    description: 'some',
    image: 'beyonce.jpeg'
  }, {
    category: dress,
    user: user,
    title: 'Evening dress',
    price: 90,
    description: 'something',
    image: 'Без названия.jpeg'
  });

  await mongoose.connection.close();
}

run().catch(e => console.error(e))